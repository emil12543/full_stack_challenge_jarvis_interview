import { IPriceFeed, PriceFeed, PricePoint } from './price_feed';
import { BehaviorSubject, ConnectableObservable, from, merge, of, Subject } from 'rxjs';
import { bufferTime, groupBy, map, max, mergeMap, min, publish, switchMap, toArray } from 'rxjs/operators';

export const enum TimeFrame {
    s1  = 1,       // 1 second
    s5  = 5,       // 5 seconds
    s15 = 15,      // 15 seconds
    s30 = 30,      // 30 seconds
    m1  = 60,      // 1 minute
    m5  = 5 * m1,  // 5 minutes
    m15 = 15 * m1, // 15 minutes
    m30 = 30 * m1, // 30 minutes
    h1  = 60 * m1, // 1 hour
    h4  = 4 * h1,  // 4 hours
    d1  = 24 * h1, // 1 day
    w1  = 7 * d1,  // 1 week
    mn1 = 30 * d1, // 1 month
}

interface IPriceSummary {
    symbol: string;
    timestamp: number;
    bestBuyPrice: {
        value: number,
        spread: number; // the difference between `value` and the sell price @ provider
        provider: string;
    };
    bestSellPrice: {
        value: number,
        spread: number; // the difference between `value` and the buy price @ provider
        provider: string;
    };
}

interface IAggregatorPriceFeed extends Omit<IPriceFeed, 'feed'> {
    feed: ConnectableObservable<IPriceFeedSummary>
}

interface IPriceFeedSummary {
    buyPrice: number,
    sellPrice: number,
    timestamp: number,
    provider: string,
    symbol: string
}

interface IPriceFeedSet {
    [providerName: string]: IPriceFeed;
}

interface IAggregatorPriceFeedSet {
    [providerName: string]: IAggregatorPriceFeed;
}

interface IPriceAggregator {
    /*
     * Creates a new `PriceAggregator` instance connected to the specified
     * array or set of price feeds.
     */
    constructor(initialPriceFeeds: (IAggregatorPriceFeed[] | IAggregatorPriceFeedSet));

    /**
     * [CORE CHALLENGE OBJECTIVE]
     * Returns `ConnectableObservable<PriceSummary>` - a stream that "ticks"
     * every `timeFrame` seconds. Each tick produces a new data point
     * containing the best buy and sell price offered during the past
     * `timeFrame` seconds.
     */
    getFeedForTimeFrame(timeFrame: TimeFrame): Subject<IPriceSummary>;

    /*
     * Adds a new price feed to the aggregator instance.
     *
     * The aggregator instance immediately connects to the newly added price feed
     * add existing users holding reference to `getFeedForTimeFrame`
     *
     * Returns `true` on success or `false` if a price feed with the same
     * `providerName` as the one passed as argument exists.
     */
    addPriceFeed(priceFeed: PriceFeed): boolean;

    /*
     * Removes the price feed with the specified `providerName`.
     *
     * Returns `true` on success or `false` if a price feed with the specified
     * name does not exist.
     */
    removePriceFeed(providerName: string): boolean;
}

export class PriceAggregator implements IPriceAggregator {
    private priceFeeds: IAggregatorPriceFeedSet;
    private summaryFeed: BehaviorSubject<IPriceSummary>;

    constructor(initialPriceFeeds: IPriceFeed[] | IPriceFeedSet) {
        let priceFeeds: IPriceFeedSet = {};
        if (!Array.isArray(initialPriceFeeds))
            priceFeeds = initialPriceFeeds;
        else {
            this.priceFeeds = {};
            initialPriceFeeds.forEach((priceFeed: PriceFeed) => priceFeeds[priceFeed.providerName] = priceFeed);
        }
        this.updatePriceFeeds(priceFeeds);
    }

    getFeedForTimeFrame(timeFrame: TimeFrame): ConnectableObservable<IPriceSummary> {
        if (!this.summaryFeed)
            this.summaryFeed = new BehaviorSubject<ConnectableObservable<IPriceSummary>>(this.getPriceFeeds())
                .pipe(
                    switchMap((observables: ConnectableObservable<IPriceFeedSummary>[]) => merge(...observables)),
                    bufferTime(timeFrame * 1000),
                    mergeMap((priceFeedSummaries: IPriceFeedSummary[]) =>
                        from(priceFeedSummaries)
                            .pipe(
                                groupBy((priceFeedSummary: IPriceFeedSummary) => priceFeedSummary.symbol),
                                mergeMap(data => data.pipe(toArray()))
                            )
                    ),
                    mergeMap((priceFeedSummaries: IPriceFeedSummary[]) =>
                        merge(
                            from(priceFeedSummaries)
                                .pipe(
                                    max((x: IPriceFeedSummary, y: IPriceFeedSummary) => x.sellPrice < y.sellPrice ? -1 : 1),
                                ),
                            from(priceFeedSummaries)
                                .pipe(
                                    min((x: IPriceFeedSummary, y: IPriceFeedSummary) => x.buyPrice < y.buyPrice ? -1 : 1)
                                )
                        )
                            .pipe(
                                toArray()
                            )
                    ),
                    mergeMap((priceFeedSummaries: IPriceFeedSummary[]) => of({
                        symbol: priceFeedSummaries[0].symbol,
                        timestamp: Date.now(),
                        bestBuyPrice: {
                            value: priceFeedSummaries[1].buyPrice,
                            spread: priceFeedSummaries[1].buyPrice - priceFeedSummaries[1].sellPrice,
                            provider: priceFeedSummaries[1].provider
                        },
                        bestSellPrice: {
                            value: priceFeedSummaries[0].sellPrice,
                            spread: priceFeedSummaries[0].buyPrice - priceFeedSummaries[0].sellPrice,
                            provider: priceFeedSummaries[0].provider
                        }
                    })),
                );

        const connectableObservable = <ConnectableObservable<IPriceSummary>> this.summaryFeed.pipe(publish());
        connectableObservable.connect();
        return connectableObservable;
    }

    addPriceFeed(priceFeed: PriceFeed): boolean {
        if (this.priceFeeds[priceFeed.providerName])
            return false;

        this.priceFeeds[priceFeed.providerName] = this.updatePriceFeed(priceFeed);
        this.passUpdatedPriceFeedsToSummaryFeed();
        return true;
    }

    removePriceFeed(providerName: string): boolean {
        if (!this.priceFeeds[providerName])
            return false;

        delete this.priceFeeds[providerName];
        this.passUpdatedPriceFeedsToSummaryFeed();
        return true;
    }

    private getPriceFeeds(): ConnectableObservable<IPriceFeedSummary>[] {
        const feeds = [];
        for (let providerName in this.priceFeeds) {
            feeds.push(this.priceFeeds[providerName].feed);
        }

        return feeds;
    }

    private updatePriceFeeds(priceFeeds: IPriceFeedSet): void {
        for (let providerName in priceFeeds) {
            this.priceFeeds[providerName] = this.updatePriceFeed(priceFeeds[providerName]);
        }
    }

    private updatePriceFeed(priceFeed: IPriceFeed): IAggregatorPriceFeed {
        return {
            ...priceFeed,
            feed: <ConnectableObservable<IPriceFeedSummary>> priceFeed.feed
                .pipe(
                    map((pricePoint: PricePoint): IPriceFeedSummary => ({
                        buyPrice: pricePoint.value.buyPrice,
                        sellPrice: pricePoint.value.sellPrice,
                        timestamp: pricePoint.timestamp,
                        provider: priceFeed.providerName,
                        symbol: priceFeed.symbol
                    }))
                )
        };
    }

    private passUpdatedPriceFeedsToSummaryFeed(): void {
        this.summaryFeed.next(this.getPriceFeeds());
    }
}
