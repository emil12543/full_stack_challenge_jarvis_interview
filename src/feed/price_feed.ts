import express, { Request, Response } from 'express';
import { PriceOffer, PricePoint } from '../price_feed';
import { ConnectableObservable, interval } from 'rxjs';
import { map, publish, repeat, timestamp } from 'rxjs/operators';
import cors from 'cors';
const app = express();
app.use(cors());

const getRandomNumber = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;

const getRandomPriceOffer = (min: number, max: number, minDifference: number, maxDifference: number): PriceOffer => {
    const buyPrice = getRandomNumber(min, max);
    let sellPrice = buyPrice - getRandomNumber(minDifference, maxDifference);

    if (sellPrice < min)
        sellPrice = min;

    return {
        buyPrice,
        sellPrice
    };
};

const randomizer = interval(+process.env.INTERVAL!)
    .pipe(
        map(() => getRandomPriceOffer(
            +process.env.MIN_VALUE!,
            +process.env.MAX_VALUE!,
            +process.env.MIN_DIFFERENCE!,
            +process.env.MAX_DIFFERENCE!)
        ),
        timestamp(),
        repeat()
    );

const connectableRandomizer = <ConnectableObservable<PricePoint>> randomizer.pipe(publish());
connectableRandomizer.connect();

app.get('/feed', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Transfer-Encoding', 'chunked');

    connectableRandomizer
        .subscribe(data => res.write(JSON.stringify(data)));
});

app.listen(process.env.PORT, () => {
    console.log('Server is running on port ' + process.env.PORT);
});
