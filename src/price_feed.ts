import { ConnectableObservable, Timestamp } from 'rxjs';

export type PriceOffer = {
    buyPrice: number;
    sellPrice: number;
};

export type PricePoint = Timestamp<PriceOffer>;

export interface IPriceFeed {
    feed: ConnectableObservable<PricePoint>;

    /*
     * The ticker symbol or trading pair represented whose price feed this
     * instance represents.
     *
     * Examples: "BTC/USD", "EUR/USD", "TSLA", etc.
     */
    symbol: string;

    /*
     * A string identifying the provider (e.g. exchange, broker, etc.) of this
     * price feed.
     */
    providerName: string;

    /*
     * Trade commission represented as percentage of trade position size (e.g.
     * `0.01` would be 1% of the position)
     */
    commission: number;
}

export class PriceFeed implements IPriceFeed {
    constructor(public feed: ConnectableObservable<PricePoint>, public symbol: string, public providerName: string, public commission: number) {
    }
}