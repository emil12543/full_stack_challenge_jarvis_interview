import React, { useEffect, useState } from 'react';
import {
    Button,
    Container,
    Row,
    Col,
    Form,
    Table,Alert
} from 'react-bootstrap';
import axios from 'axios';

const ENDPOINT = process.env.REACT_APP_AGGREGATOR_ENDPOINT!
const eventSource = new EventSource(`${ENDPOINT}/feeds`);
const App: React.FC = () => {
    const { value: url, bind: bindUrl, reset: resetUrl } = useInput('');
    const { value: provider, bind: bindProvider, reset: resetProvider } = useInput('');
    const { value: symbol, bind: bindSymbol, reset: resetSymbol } = useInput('');
    const { value: commission, bind: bindCommission, reset: resetCommission } = useInput('');
    const [loading, setLoading] = useState(false);
    const [providers, setProviders] = useState<any>([]);
    const [messages, setMessages] = useState<any>([]);
    const [alertShow, setAlertShow] = useState(false);

    const addProvider = async() => {
        setLoading(true);
        const response = await axios
            .post(
                `${process.env.REACT_APP_AGGREGATOR_ENDPOINT!}/feed`,
                {
                    url,
                    provider_name: provider,
                    symbol,
                    commission
                }
            );
        if (response.data.data) {
            setProviders([
                ...providers,
                {
                    url,
                    provider_name: provider,
                    symbol,
                    commission
                }
            ]);
            resetUrl();
            resetProvider();
            resetSymbol();
            resetCommission();
        } else {
            setAlertShow(true);
            setTimeout(() => {
                setAlertShow(false);
            }, 2000);
        }

        setLoading(false);
    };

    const removeProvider = async (provider_name: string) => {
        const response = await axios
            .delete(
                `${process.env.REACT_APP_AGGREGATOR_ENDPOINT!}/feed/${provider_name}`
            );

        if (response.data.data) {
            setProviders([...providers.filter((provider: any) => provider.provider_name !== provider_name)]);
        }
    };

    useEffect(() => {
        const listener = (event: MessageEvent) => {
            setMessages([JSON.parse(event.data), ...messages])
        };
        eventSource.addEventListener('message', listener);

        return () => {
            eventSource.removeEventListener('message', listener)
        }
    }, [messages]);

  return (
    <Container className="p-3">
        <Row>
            <Col>
                <Form>
                    <Form.Group as={Row} controlId="formURL">
                        <Form.Label column sm="3">
                            URL
                        </Form.Label>
                        <Col sm="9">
                            <Form.Control placeholder="http://localhost:8080" {...bindUrl} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formProviderName"  >
                        <Form.Label column sm="3">
                            Provider name
                        </Form.Label>
                        <Col sm="9">
                            <Form.Control placeholder="Jarvis" {...bindProvider} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formSymbol">
                        <Form.Label column sm="3">
                            Symbol
                        </Form.Label>
                        <Col sm="9">
                            <Form.Control placeholder="BTC/USD" {...bindSymbol} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row} controlId="formCommission">
                        <Form.Label column sm="3">
                            Commission
                        </Form.Label>
                        <Col sm="9">
                            <Form.Control placeholder="0.01" {...bindCommission} />
                        </Col>
                    </Form.Group>

                    <Form.Group as={Row}>
                        <Col>
                            <Button block onClick={() => addProvider()} disabled={loading}>Add</Button>
                        </Col>
                    </Form.Group>
                </Form>
                {alertShow &&
                    <Alert variant="danger" onClose={() => setAlertShow(false)} dismissible>
                        <Alert.Heading>There is already provider with this name</Alert.Heading>
                    </Alert>
                }
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>URL</th>
                        <th>Provider</th>
                        <th>Symbol</th>
                        <th>Commission</th>
                    </tr>
                    </thead>
                    <tbody>
                    {providers.map(({url, provider_name, symbol, commission}: any, index: number) => (
                        <tr key={provider_name}>
                            <td><Button variant="danger" onClick={() => removeProvider(provider_name)}>x</Button></td>
                            <td>{url}</td>
                            <td>{provider_name}</td>
                            <td>{symbol}</td>
                            <td>{commission}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </Col>
            <Col>
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        <th>#/#</th>
                        <th>Time</th>
                        <th>Best buy</th>
                        <th>Best sell</th>
                    </tr>
                    </thead>
                    <tbody>
                    {messages.map((message: any) => (
                        <tr key={`${message.symbol}-${message.timestamp}`}>
                            <td>{message.symbol}</td>
                            <td>{new Date(message.timestamp).toLocaleTimeString()}</td>
                            <td>{message.bestBuyPrice.value} {message.bestBuyPrice.provider}</td>
                            <td>{message.bestSellPrice.value} {message.bestSellPrice.provider}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </Col>
        </Row>
    </Container>
  );
};

const useInput = (initialValue: string) => {
    const [value, setValue] = useState(initialValue);

    return {
        value,
        setValue,
        reset: () => setValue(""),
        bind: {
            value,
            onChange: (event: any) => {
                setValue(event.target.value);
            }
        }
    };
};

export default App;
