import express, { NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';
import { PriceAggregator, TimeFrame } from '../price_aggregator';
import { ConnectableObservable, Subject } from 'rxjs';
import { publish } from 'rxjs/operators';
import request from 'request';
import cors from 'cors'
import { PriceFeed, PricePoint } from '../price_feed';

const app = express();
const port = process.env.PORT;
app.use(bodyParser());
app.use(cors());

const aggregator = new PriceAggregator([]);
const observable = aggregator.getFeedForTimeFrame(TimeFrame.s5);

app.get('/feeds', (req: Request, res: Response, next: NextFunction) => {
    const headers = {
        'Content-Type': 'text/event-stream',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache'
    };
    res.writeHead(200, headers);

    observable.subscribe(data => res.write(`data: ${JSON.stringify(data)}\n\n`));
});

app.post('/feed', (req: Request, res: Response) => {

    const subject = new Subject<PricePoint>();
    request
        .get(req.body.url)
        .on('data', (data: string) => {
            subject.next(JSON.parse(Buffer.from(data).toString()))
        });

    const feed = <ConnectableObservable<PricePoint>> subject.pipe(publish());
    feed.connect();
    const result = aggregator.addPriceFeed(new PriceFeed(feed, req.body.symbol, req.body.provider_name, req.body.commission));
    res
        .json({data: result})
        .end();
});

app.delete('/feed/:provider_name', (req: Request, res: Response) => {
    const result = aggregator.removePriceFeed(req.params.provider_name);
    res
        .json({
            data: result
        })
        .end();
});

app.listen(port, () => {
    console.log('Server is running on port ' + port);
});
